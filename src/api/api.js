import axios from "axios"



const instance = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL
})



export const getAllSkills = async () => {

    try {
        const res = await instance.get('skills')
        return res.data
    } catch (err) {
        console.log(err)
    }
}


export const submitForm = async (data) => {

    try {
        const res = instance.post('application', { ...data, token: process.env.REACT_APP_TOKEN })
        return res.data

    } catch (err) {
        console.log(err)
    }
}

export const getSubmitedForms = async () => {

    try {
        const res = await instance.get(`applications?token=${process.env.REACT_APP_TOKEN}`)
        return res.data

    } catch (err) {
        console.log(err)
    }
}