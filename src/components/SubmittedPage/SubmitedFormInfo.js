import PersonalInfo from "./PersonalInfo"
import CovidInfo from "./CovidInfo"
import SkillsInfo from "./SkillsInfo"
import DevtalkInfo from "./DevtalkInfo"



const SubmitedFormInfo = ({ open, data, skills }) => {

    return <div className={`submited__form--info-wrapper ${open && "submited__form--info-open"}`}
    >
        <div className="submited__form--left">

            <PersonalInfo data={data} />
            <CovidInfo data={data} />

        </div>
        <div className="submited__form--right">

            <SkillsInfo skills={skills} data={data} />
            <DevtalkInfo data={data} />

        </div>


    </div>

}

export default SubmitedFormInfo