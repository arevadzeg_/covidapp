import FilledRadioInput from "./FilledInputs/FilledRadioInput"



const DevtalkInfo = ({ data }) => {
    return <div className="submited__form--devtalk">
        <p className="application__header">Insigts</p>
        <p className='submited__form--question'>Would you attend Devtalks and maybe also organize your own?</p>
        <div className="submited__form--covid-radio">
            <FilledRadioInput title='Yes' value={true} data={data.will_organize_devtalk} />
            <FilledRadioInput title='No' value={false} data={data.will_organize_devtalk} />
        </div>
        {
            data.will_organize_devtalk &&
            <>
                <p className='submited__form--question'>What would you speak about at Devtalk?</p>
                <div className='submited__form--textarea submited__form--textarea-devtalk'>
                    {data.devtalk_topic}
                </div>
            </>
        }
        <p className='submited__form--question'>Tell us somthing special</p>
        <div className='submited__form--textarea'>
            {data.something_special}
        </div>
    </div>
}

export default DevtalkInfo