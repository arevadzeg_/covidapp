import DatePickerSVG from "../../../assets/svgs/DatepickerSVG"



const FilledDatePicker = ({ date }) => {


    return (
        <div className='submited__form--date'>
            {date}
            <DatePickerSVG />
        </div>
    )
}

export default FilledDatePicker