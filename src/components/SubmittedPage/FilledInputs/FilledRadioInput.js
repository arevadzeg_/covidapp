import RadioButtonCheckedSVG from "../../../assets/svgs/RadioButtonCheckedSVG"
import RadioButtonSVG from "../../../assets/svgs/RadioButtonSVG"



const FilledRadioInput = ({ title, value, data }) => {

    return (
        <div >
            {data === value ? <RadioButtonCheckedSVG /> : <RadioButtonSVG />}
            <span>{title}</span>
        </div>
    )
}

export default FilledRadioInput