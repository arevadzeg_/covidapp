



const SkillsInfo = ({ skills, data }) => {
    return <div className="submited__form--skillset">
        <p className="application__header">Skillset</p>
        <div>
            <div>
                {
                    data.skills.map((skill) => {
                        return <p key={skill.id}>{skills.find((singleSkill) => singleSkill?.id === skill?.id)?.title}</p>
                    })
                }
            </div>
            <div>
                {
                    data.skills.map((skill) => {
                        return <p key={skill.id}>{"Years of experiance: " + skill.experience}</p>
                    })
                }
            </div>
        </div>
    </div>
}

export default SkillsInfo