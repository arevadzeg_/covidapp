import FilledDatePicker from "./FilledInputs/FilledDatePicker"
import FilledRadioInput from "./FilledInputs/FilledRadioInput"



const CovidInfo = ({ data }) => {
    return (
        <div className="submited__form--covid-info">
            <p className="application__header">Covid Situation</p>

            <p className='submited__form--question'>how would you prefer to work ?</p>
            <div className="submited__form--covid-radio">
                <FilledRadioInput title='From Sairme Office' value='from_office' data={data.work_preference} />
                <FilledRadioInput title='From Home' value='from_home' data={data.work_preference} />
                <FilledRadioInput title='Hybrid' value='hybrid' data={data.work_preference} />
            </div>

            <p className='submited__form--question'>Did you have covid 19 ?</p>
            <div className="submited__form--covid-radio">
                <FilledRadioInput title='Yes' value={true} data={data.had_covid} />
                <FilledRadioInput title='No' value={false} data={data.had_covid} />
            </div>
            {
                data.had_covid && <>
                    <p className='submited__form--question'>When did you have covid 19 ?</p>
                    <FilledDatePicker date={data.had_covid_at} />
                </>
            }
            <p className='submited__form--question'>Have you been vaccinated ?</p>
            <div className="submited__form--covid-radio">
                <FilledRadioInput title='Yes' value={true} data={data.vaccinated} />
                <FilledRadioInput title='No' value={false} data={data.vaccinated} />

            </div>
            {data.vaccinated && <>
                <p className='submited__form--question'>When did you get covid vaccine ?</p>
                <FilledDatePicker date={data.vaccinated_at} />
            </>
            }
        </div>
    )
}

export default CovidInfo