

const PersonalInfo = ({ data }) => {
    return (
        <div className="submited__form--personal-info">
            <p className="application__header">Personal Information</p>
            <div>
                <div className="submited__form--personal-info-keys">
                    <p>Fist Name</p>
                    <p>Last Name</p>
                    <p>E Mail</p>
                    {data.phone && <p>Phone</p>}
                </div>
                <div className="submited__form--personal-info-values">
                    <p>{data.first_name}</p>
                    <p>{data.last_name}</p>
                    <p>{data.email}</p>
                    {data.phone && <p>{data.phone}</p>}
                </div>

            </div>
        </div>
    )
}

export default PersonalInfo