import { useState } from "react"
import { useNavigate } from "react-router-dom";
import { submitForm } from "../../api/api";



const SubmitPage = ({ setActiveStep, formData }) => {


    const [showThankYouMessage, setShowThankYouMessage] = useState(false)


    const navigate = useNavigate()


    const handleBack = () => {
        setActiveStep((prev) => prev -= 1)
    }




    const handleSubmit = async () => {
        if (!formData.vaccinated) delete formData.vaccinated_at
        if (!formData.had_covid) delete formData.had_covid_at
        if (!formData.devtalk_topic) delete formData.devtalk_topic
        if (formData.phone.length === 6) delete formData.phone
        const res = await submitForm(formData)
        setShowThankYouMessage(true)

        setTimeout(() => {
            navigate('/')
        }, 3000)

    }



    return <div className="submit__page">

        {
            showThankYouMessage ?
                <h1>Thanks for Joining 😊</h1> :
                <>
                    <button className="btn btn-orange" onClick={handleSubmit}>Submit</button>
                    <button className="btn btn-white" onClick={handleBack}>go back</button>
                </>
        }
    </div>
}

export default SubmitPage