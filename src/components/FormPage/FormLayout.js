import React from 'react';


const FormLayout = ({ header, children }) => {


    return <div>
        <div className="form__header">
            {header}
        </div>
        <div className="form__body">
            {children}
        </div>
    </div>
}

export default FormLayout