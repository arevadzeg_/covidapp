import { MenuItem, Select, TextField } from "@mui/material"
import { useEffect, useState } from "react"
import { getAllSkills } from "../../../api/api"
import DeleteSvg from '../../../assets/svgs/DeleteSVG'
import Stepper from ".././Stepper"
import '../Styles/SkillsForm.scss'

const SkillsForm = ({ setActiveStep, activeStep, addAllInfo, formData }) => {

    const [skills, setSkills] = useState([])
    const [selectedSkill, setSelectedSkill] = useState('')
    const [selectedSkills, setSelectedSkills] = useState(formData.skills)
    const [experience, setExperience] = useState('')
    const [error, setError] = useState({})


    useEffect(() => {
        getAllSkills().then((res) => {
            setSkills(res)
        })
    }, [])

    const changeSelectedSkill = (skill) => {
        setSelectedSkill(skill.target.value)
    }


    const addSelectedSkill = () => {
        if (experience) {
            setSelectedSkills((prevSkills) => [...prevSkills, { ...selectedSkill, experience: experience }])
            setSelectedSkill("")
            setExperience('')
            setError({})
        } else {
            setError({ experiance: '* Please enter experiance' })
        }
    }

    const deleteSkill = ({ id }) => {
        setSelectedSkills((allSkils) => allSkils.filter((skill) => {
            return skill.id !== id
        }))

    }

    const submitForm = () => {
        if (selectedSkills.length === 0) {
            setError({ skill: "* please select at least one skill" })
        } else {
            addAllInfo({ skills: selectedSkills })
            setActiveStep((prev) => prev += 1)
        }
    }

    const handleExperianceInputPress = (e) => {
        if (isNaN(Number(e.key)) || experience.length >= 2) {
            e.preventDefault()
        }
    }


    return <div>



        <form className="skills__form">

            <Select
                value={''}
                displayEmpty={true}
                onChange={changeSelectedSkill}
                renderValue={value => 'Skills'}
                className='skill__form--dropdown'
            >

                {
                    skills
                        .filter(skill => {
                            if (skill.id === selectedSkill) return false

                            for (let i = 0; i < selectedSkills.length; i++) {
                                if (skill.id === selectedSkills[i].id) return false
                            }
                            return true
                        })
                        .map((skill => {
                            return <MenuItem key={skill.id} value={skill} >{skill.title}</MenuItem>
                        }))
                }
            </Select>
            <p className="skill__form--error">{error?.skill}</p>

            {
                selectedSkill &&
                <>
                    <TextField
                        placeholder="Experience Duration in Years"
                        className='default__input'
                        variant="standard"
                        type='text'
                        onKeyPress={(e) => handleExperianceInputPress(e)}
                        value={experience}
                        onChange={(e) => setExperience(e.target.value)}
                    />
                    <p className="skill__form--error">{error?.experiance}</p>
                    <div className="skill__experiance--input">

                        <button className="btn btn__add--skill" type='button' onClick={addSelectedSkill}>
                            Add Programming Language
                        </button>
                    </div>
                </>
            }


            <div className="skill__selected--wrapper">

                {
                    selectedSkills.map((skill) => {
                        return <div className="skill__selected" key={skill.id}>
                            <div className="skill__selected--title">{skill.title}</div>
                            <div className="skill__selected--experiance">Years of Experience: {skill.experience} </div>
                            <DeleteSvg onClick={() => deleteSkill({ id: skill.id, title: skill.title })} />
                        </div>
                    })
                }
            </div>


        </form>

        <Stepper activeStep={activeStep} setActiveStep={setActiveStep} nextClick={submitForm} />

    </div>
}

export default SkillsForm