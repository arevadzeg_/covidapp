import { TextField } from "@mui/material"
import { useFormik } from "formik"
import Stepper from "../Stepper"
import { CoordinatsSchema } from './validationSchema'
import '../Styles/CoordinatesForm.scss'
import { useState } from "react"

const CoordinatesForm = ({ addAllInfo, activeStep, setActiveStep, formData }) => {


    const [phoneError, setPhoneError] = useState(null)


    const formik = useFormik({
        initialValues: {
            first_name: formData.first_name,
            last_name: formData.last_name,
            email: formData.email,
            phone: formData.phone,
        },
        onSubmit: (values) => {
            addAllInfo(values)
            setActiveStep((prev) => prev += 1)
        },
        validationSchema: CoordinatsSchema,
        validateOnBlur: false,
        validateOnChange: false
    });

    const handleSubmit = () => {
        if (formik.values.phone.length === 6 || formik.values.phone.length === 14) {
            setPhoneError(null)
            formik.submitForm()
        } else {
            setPhoneError('* pelase enter a valid phone')
        }
    }



    const handlePhoneInpt = (e) => {
        if (e.target.value.length < 6 || e.target.value.length > 14) {
        }
        else if (isNaN(Number(e.nativeEvent.data))) {
        }
        else {
            formik.handleChange(e)
        }
    }

    return (
        <div>

            <form className="coordinates__form">
                <TextField
                    error={Boolean(formik.errors.first_name)}
                    helperText={formik.errors.first_name}
                    placeholder="First Name"
                    name='first_name'
                    className='default__input'
                    variant="standard"
                    value={formik.values.first_name}
                    onChange={formik.handleChange}
                />
                <TextField
                    error={Boolean(formik.errors.last_name)}
                    helperText={formik.errors.last_name}
                    placeholder="Last Name"
                    name='last_name'
                    className='default__input'
                    variant="standard"
                    value={formik.values.last_name}
                    onChange={formik.handleChange}


                />
                <TextField
                    error={Boolean(formik.errors.email)}
                    helperText={formik.errors.email}
                    placeholder="E Mail"
                    name='email'
                    className='default__input'
                    variant="standard"
                    value={formik.values.email}
                    onChange={formik.handleChange}

                />
                <TextField
                    error={Boolean(phoneError)}
                    helperText={phoneError}
                    name='phone'
                    className={`default__input ${formik.values.phone.length < 7 && "default__input--phone"}`}
                    variant="standard"
                    value={formik.values.phone}
                    onChange={(e) => handlePhoneInpt(e)}

                />
            </form>

            <Stepper activeStep={activeStep} setActiveStep={setActiveStep} nextClick={handleSubmit} />
        </div>

    )
}

export default CoordinatesForm