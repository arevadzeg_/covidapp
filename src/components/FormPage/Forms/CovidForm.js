import { FormControl, FormControlLabel, FormHelperText, Radio, RadioGroup } from "@mui/material"
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import DatePickerSVG from "../../../assets/svgs/DatepickerSVG";
import RadioButtonSVG from "../../../assets/svgs/RadioButtonSVG";
import RadioButtonCheckedSVG from "../../../assets/svgs/RadioButtonCheckedSVG";
import { useFormik } from "formik";
import moment from "moment";
import { CovidValidationSchema } from "./validationSchema";
import Stepper from "../Stepper";
import '../Styles/CovidForm.scss'


const CovidForm = ({ setActiveStep, activeStep, addAllInfo, formData }) => {

    const formik = useFormik({
        initialValues: {
            work_preference: formData.work_preference,
            had_covid: formData.had_covid,
            vaccinated: formData.vaccinated,
            had_covid_at: formData.had_covid_at,
            vaccinated_at: formData.vaccinated_at,
        },
        validationSchema: CovidValidationSchema,
        validateOnBlur: false,
        validateOnChange: false,
        onSubmit: (values) => {
            setActiveStep((step) => step += 1)
            addAllInfo(values)
        }
    })





    return (
        <div>

            <form className="covid__form">
                <p>how would you prefer to work?</p>
                <FormControl error={Boolean(formik.errors.work_preference)}

                >

                    <RadioGroup
                        name="work_preference"
                        onChange={formik.handleChange}
                        value={formik.values.work_preference}

                    >
                        <FormControlLabel
                            value="from_office"
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>From Sairme Office</span>} />
                        <FormControlLabel
                            value="from_home"
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>From Home</span>} />
                        <FormControlLabel
                            value="hybrid"
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>Hybrid</span>} />
                    </RadioGroup>
                    <FormHelperText>{formik.errors.work_preference}</FormHelperText>
                </FormControl>

                <p>Did you contact covid 19? :(</p>
                <FormControl
                    error={Boolean(formik.errors.had_covid)}>

                    <RadioGroup
                        name="had_covid"
                        onChange={(e) => formik.setFieldValue('had_covid', e.target.value === 'true' ? true : false)}
                        value={formik.values.had_covid}
                    >
                        <FormControlLabel
                            value={true}
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>Yes</span>} />
                        <FormControlLabel
                            value={false}
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>No</span>} />
                    </RadioGroup>
                    <FormHelperText>{formik.errors.had_covid}</FormHelperText>

                </FormControl>

                {
                    formik.values.had_covid === true &&
                    <>
                        <p>When?</p>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker
                                components={{
                                    OpenPickerIcon: DatePickerSVG
                                }}
                                className='default__datepicker'
                                name="had_covid_at"
                                onChange={(e) => formik.setFieldValue('had_covid_at', moment(e).format('YYYY-MM-DD'))}
                                value={formik.values.had_covid_at}
                                renderInput={(params) => <TextField {...params} error={Boolean(formik.errors.had_covid_at)} helperText={formik.errors.had_covid_at} />}
                            />
                        </LocalizationProvider>
                    </>
                }

                <p>
                    Have you been vaccinated?
                </p>

                <FormControl
                    error={Boolean(formik.errors.vaccinated)}
                >
                    <RadioGroup
                        name="vaccinated"
                        onChange={(e) => formik.setFieldValue('vaccinated', e.target.value === 'true' ? true : false)}
                        value={formik.values.vaccinated}

                    >
                        <FormControlLabel
                            value={true}
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>Yes</span>} />
                        <FormControlLabel
                            value={false}
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>No</span>} />
                    </RadioGroup>
                    <FormHelperText>{formik.errors.vaccinated}</FormHelperText>

                </FormControl>

                {
                    formik.values.vaccinated === true && <>
                        <p>When did you get your last covid vaccine?</p>
                        <LocalizationProvider dateAdapter={AdapterDateFns}>
                            <DatePicker
                                components={{
                                    OpenPickerIcon: DatePickerSVG
                                }}
                                className='default__datepicker'
                                onChange={(e) => formik.setFieldValue('vaccinated_at', moment(e).format('YYYY-MM-DD'))}
                                value={formik.values.vaccinated_at}
                                name="vaccinated_at"
                                renderInput={(params) => <TextField {...params} error={Boolean(formik.errors.vaccinated_at)} helperText={formik.errors.vaccinated_at} />}
                            />
                        </LocalizationProvider>
                    </>
                }


            </form>
            <Stepper activeStep={activeStep} setActiveStep={setActiveStep} nextClick={formik.handleSubmit} />

        </div >
    )
}

export default CovidForm