import * as Yup from 'yup';

export const CoordinatsSchema = Yup.object().shape({
  first_name: Yup.string()
    .min(2, '* first name should include 2 or more characters')
    .max(50, '* first name too long')
    .required('* first name is required'),
  last_name: Yup.string()
    .min(2, '* last name should include 2 or more characters')
    .max(50, '* last name too long')
    .required('* last name is required'),
  email: Yup.string().email('Invalid email').required('* email is reqired'),
});


export const CovidValidationSchema = Yup.object().shape({
  work_preference: Yup.string().required("Field reqired"),
  had_covid: Yup.boolean().required("Field reqired").typeError('Field reqired'),
  vaccinated: Yup.boolean().required("Field reqired").typeError('Field reqired'),
  had_covid_at: Yup.date().nullable().when("had_covid", {
    is: true,
    then: Yup.date().typeError('Please select date').required("Please select the date")
  }),
  vaccinated_at: Yup.date().nullable().when("vaccinated", {
    is: true,
    then: Yup.date().typeError('Please select date').required("Please select the date")
  }),
})


export const DevltalkValidationSchema = Yup.object().shape({
  will_organize_devtalk: Yup.boolean().required('Field reqired').typeError('Field reqired'),
  devtalk_topic: Yup.string().when("will_organize_devtalk", {
    is: true,
    then: Yup.string().required("Please tell us the topic")
  }),
  something_special: Yup.string().required('You must be special!')
})