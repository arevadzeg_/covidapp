import { FormControl, FormControlLabel, FormHelperText, Radio, RadioGroup } from "@mui/material"
import { useFormik } from "formik"
import RadioButtonCheckedSVG from "../../../assets/svgs/RadioButtonCheckedSVG"
import RadioButtonSVG from "../../../assets/svgs/RadioButtonSVG"
import Stepper from "../Stepper"
import { DevltalkValidationSchema } from "./validationSchema"
import '../Styles/DevTalkForm.scss'




const DevtalkForm = ({
    addAllInfo,
    activeStep,
    setActiveStep,
    formData
}) => {




    const formik = useFormik({
        initialValues: {
            will_organize_devtalk: formData.will_organize_devtalk,
            devtalk_topic: formData.devtalk_topic,
            something_special: formData.something_special
        },
        validationSchema: DevltalkValidationSchema,
        validateOnBlur: false,
        validateOnChange: false,
        onSubmit: (values) => {
            addAllInfo(values)
            setActiveStep((prev) => prev += 1)
        }
    })


    return (
        <div>

            <form className="devtalk__form">
                <p>Would you attend Devtalks and maybe also organize your own?</p>
                <FormControl error={Boolean(formik.errors.will_organize_devtalk)}>

                    <RadioGroup
                        name="will_organize_devtalk"
                        onChange={(e) => formik.setFieldValue('will_organize_devtalk', e.target.value === 'true' ? true : false)}
                        value={formik.will_organize_devtalk}
                    >
                        <FormControlLabel
                            value={true}
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>Yes</span>} />
                        <FormControlLabel
                            value={false}
                            control={<Radio
                                icon={<RadioButtonSVG />}
                                checkedIcon={<RadioButtonCheckedSVG />}
                                disableRipple />}
                            label={<span className='covid__form--radio-label'>No</span>} />

                    </RadioGroup>
                    <FormHelperText>{formik.errors.will_organize_devtalk}</FormHelperText>
                </FormControl>

                {
                    formik.values.will_organize_devtalk && <FormControl error={Boolean(formik.errors.devtalk_topic)}>
                        <p>What would you speak about at Devtalk?</p>
                        <textarea
                            type='text'
                            className={`default__textarea textarea__topic ${formik.errors.devtalk_topic && 'default__textarea--error'}`}
                            placeholder="I would..."
                            name="devtalk_topic"
                            value={formik.values.devtalk_topic}
                            onChange={formik.handleChange} />
                        <FormHelperText>{formik.errors.devtalk_topic}</FormHelperText>
                    </FormControl>
                }



                <FormControl error={Boolean(formik.errors.something_special)}>
                    <p>Tell us something special</p>
                    <textarea
                        type='text'
                        placeholder="I..."
                        className={`default__textarea textarea__special ${formik.errors.something_special && 'default__textarea--error'}`}
                        name='something_special'
                        value={formik.values.something_special}
                        onChange={formik.handleChange} />
                    <FormHelperText>{formik.errors.something_special}</FormHelperText>
                </FormControl>



            </form>



            <Stepper activeStep={activeStep} setActiveStep={setActiveStep} nextClick={formik.submitForm} />

        </div>
    )
}

export default DevtalkForm

