import { useNavigate } from "react-router-dom";
import NextPrevSVG from "../../assets/svgs/NextPrevSVG"



const Stepper = ({ setActiveStep, activeStep, nextClick }) => {

    const navigate = useNavigate()

    const handleBack = () => {
        if (activeStep > 0) setActiveStep((prevActiveStep) => prevActiveStep - 1);
        else {
            navigate('/')
        }
    };

    const handleDotClick = (index) => {
        if (activeStep > index) {
            setActiveStep(index)
        }
    }

    return (
        <div className="form__footer">
            <NextPrevSVG className='form__prev' onClick={handleBack} />
            {
                [...Array(5)].map((item, index) => {
                    return <div
                        className={`form__step ${index <= activeStep && "form__step--fullfilled"}`}
                        key={index}
                        onClick={() => handleDotClick(index)}>
                    </div>
                })
            }
            <NextPrevSVG className='form__next' onClick={nextClick} />
        </div>
    )

}


export default Stepper