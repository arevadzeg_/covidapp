

const InfoLayout = ({ header, body }) => {
    return <div>
        <div className="info__header">
            {header}
        </div>
        <div className='info__body'>
            {body}
        </div>
    </div >
}

export default InfoLayout