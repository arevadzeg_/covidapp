

const RadioButtonSVG = () => {

    return <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 14 14" fill="none">
        <path d="M1 7C1 10.3137 3.68629 13 7 13C10.3137 13 13 10.3137 13 7C13 3.68629 10.3137 1 7 1C3.68629 1 1 3.68629 1 7Z" stroke="url(#paint0_radial_49_27)" strokeLinecap="round" strokeLinejoin="round" />
        <defs>
            <radialGradient id="paint0_radial_49_27" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(7 7) rotate(90) scale(6)">
                <stop stopColor="#0C0D0E" />
                <stop offset="0.734375" stopColor="#151718" />
            </radialGradient>
        </defs>
    </svg>
}

export default RadioButtonSVG