

const RadioButtonCheckedSVG = () => {

    return <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
        <path d="M1 7.5C1 11.0899 3.91015 14 7.5 14C11.0899 14 14 11.0899 14 7.5C14 3.91015 11.0899 1 7.5 1C3.91015 1 1 3.91015 1 7.5Z" stroke="url(#paint0_radial_49_52)" strokeLinecap="round" strokeLinejoin="round" />
        <path d="M5.0625 7.5C5.0625 8.84619 6.15381 9.9375 7.5 9.9375C8.84619 9.9375 9.9375 8.84619 9.9375 7.5C9.9375 6.15381 8.84619 5.0625 7.5 5.0625C6.15381 5.0625 5.0625 6.15381 5.0625 7.5Z" stroke="url(#paint1_radial_49_52)" strokeLinecap="round" strokeLinejoin="round" />
        <defs>
            <radialGradient id="paint0_radial_49_52" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(7.5 7.5) rotate(90) scale(6.5)">
                <stop stopColor="#0C0D0E" />
                <stop offset="0.734375" stopColor="#151718" />
            </radialGradient>
            <radialGradient id="paint1_radial_49_52" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(7.5 7.5) rotate(90) scale(6.5)">
                <stop stopColor="#0C0D0E" />
                <stop offset="0.734375" stopColor="#151718" />
            </radialGradient>
        </defs>
    </svg>
}

export default RadioButtonCheckedSVG