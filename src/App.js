import {
    Routes,
    Route,
} from "react-router-dom";
import Form from "./routes/Form";
import SubmitedApplications from "./routes/SubmitedApplications";
import Home from "./routes/Home";
import './App.scss'


const App = () => {

    return (
        <div>
            <Routes >
                <Route path="/submited" element={<SubmitedApplications />} />
                <Route path="/form" element={<Form />} />
                <Route path="/" element={<Home />} />
            </Routes >
        </div>

    )
}

export default App