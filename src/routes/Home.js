import { Link } from 'react-router-dom';


function Home() {
  return (
    <div className="App">
      <main className='main'>
        <h1 className='main__welcome'>
          Welcome Rocketeer !
        </h1>
        <div >
          <Link to='/form'>
            <button className='btn btn-orange'>
              Start Questionnaire
            </button>
          </Link>
          <Link to='/submited'>
            <button className='btn btn-white'>
              Submitted Applications
            </button>
          </Link>
        </div>
        <img src='rocketman.png' className='rocketman' />
      </main>
    </div>
  );
}

export default Home;
