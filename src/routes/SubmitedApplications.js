import { CircularProgress } from '@mui/material'
import { useEffect, useState } from 'react'
import { getAllSkills, getSubmitedForms } from '../api/api'
import DropdownSVG from '../assets/svgs/DropDownSVG'
import '../components/SubmittedPage/Styles/submited.scss'
import SubmitedFormInfo from '../components/SubmittedPage/SubmitedFormInfo'


const SubmitedApplications = () => {

    const [allApplications, setAllApplications] = useState([])
    const [loading, setLoading] = useState(true)
    const [opened, setOpened] = useState(null)
    const [skills, setSkills] = useState([])


    const handleOpenAplication = (index) => {
        if (index === opened) {
            setOpened(null)
        } else {
            setOpened(index)
        }
    }


    useEffect(() => {
        getSubmitedForms().then((res) => {
            setAllApplications(res)
            setLoading(false)

        }).catch((err) => {
            console.log(err)
            setLoading(false)

        })
        getAllSkills().then((res) => {
            setSkills(res)
        })

    }, [])


    return (
        <div className="submited__aplications">
            <h1>Submitted Applications</h1>

            {
                loading ? <CircularProgress /> :
                    <div>
                        {
                            allApplications.length === 0 ? <h2>
                                No Applications :(
                            </h2> : <>
                                {allApplications.map((entry, index) =>
                                    <div className='submited__aplication--wrapper' key={index}>
                                        <div className={`submited__aplication ${index === opened && 'submited__aplication--opened'}`} onClick={() => handleOpenAplication(index)}>
                                            <span>{index + 1}</span>
                                            <DropdownSVG />
                                        </div>
                                        <SubmitedFormInfo open={opened === index} data={entry} skills={skills} />
                                    </div>
                                )
                                }
                            </>
                        }
                    </div>
            }


        </div>
    )
}

export default SubmitedApplications