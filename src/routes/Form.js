import '../components/FormPage/Styles/form.scss'
import InfoLayout from '../components/FormPage/InfoLayout'
import FormLayout from '../components/FormPage/FormLayout'
import CoordinatesForm from '../components/FormPage/Forms/CoordinatesForm'
import { useState } from 'react'
import SkillsForm from '../components/FormPage/Forms/SkillsForm'
import CovidForm from '../components/FormPage/Forms/CovidForm'
import DevTalkform from '../components/FormPage/Forms/DevTalkForm'
import SubmitPage from '../components/FormPage/SubmitPage'


const Form = () => {
    const [activeStep, setActiveStep] = useState(0);
    const [formData, setFormData] = useState({
        "token": "",
        "first_name": "",
        "last_name": "",
        "email": "",
        "phone": "+995 5",
        "skills": [],
        "work_preference": "",
        "had_covid": null,
        "had_covid_at": null,
        "vaccinated": null,
        "vaccinated_at": null,
        "will_organize_devtalk": null,
        "devtalk_topic": "",
        "something_special": ""
    })

    const addAllInfo = (data) => {
        setFormData((prev) => {
            return { ...prev, ...data }
        }
        )
    }





    const getStepForm = (step) => {
        switch (step) {
            case 0:
                return {
                    component:
                        <CoordinatesForm
                            addAllInfo={addAllInfo}
                            activeStep={activeStep}
                            setActiveStep={setActiveStep}
                            formData={formData} />,
                    header: 'Hey, Rocketeer, what are your coordinates?'
                };
            case 1:
                return {
                    component: <SkillsForm
                        addAllInfo={addAllInfo}
                        activeStep={activeStep}
                        setActiveStep={setActiveStep}
                        formData={formData} />,
                    header: 'Tell us about your skills'
                };
            case 2:
                return {
                    component: <CovidForm
                        addAllInfo={addAllInfo}
                        activeStep={activeStep}
                        setActiveStep={setActiveStep}
                        formData={formData}
                    />,
                    header: 'Covid Stuff'

                };
            case 3:
                return {
                    component: <DevTalkform
                        addAllInfo={addAllInfo}
                        activeStep={activeStep}
                        setActiveStep={setActiveStep}
                        formData={formData} />,
                    header: "What about you?"
                }
            default:
                return null;
        }
    };
    const getStepInfo = (step) => {
        switch (step) {
            case 0:
                return {
                    header: 'Redberry Origins',
                    body: 'You watch “What? Where? When?” Yeah. Our founders used to play it. That’s where they got a question about a famous American author and screenwriter Ray Bradbury. Albeit, our CEO Gaga Darsalia forgot the exact name and he answered Ray Redberry. And at that moment, a name for a yet to be born company was inspired - Redberry 😇'
                }
            case 1:
                return {
                    header: 'A bit about our battles',
                    body: 'As we said, Redberry has been on the field for quite some time now, and we have touched and embraced a variety of programming languages, technologies, philosophies, and frameworks. We are battle-tested in PHP Laravel Stack with Vue.js, refined in React, and allies with Serverside technologies like Docker and Kubernetes, and now we have set foot in the web3 industry.',
                }
            case 2:
                return {
                    header: 'Redberry Covid Policies',
                    body: 'As this infamous pandemic took over the world, we adjusted our working practices so that our employees can be as safe and comfortable as possible. We have a hybrid work environment, so you can either work from home or visit our lovely office on Sairme Street. If it was up to us, we would love you to see us in the office because we think face-to-face communications > Zoom meetings. Both on the fun and productivity scales. '
                }
            case 3:
                return {
                    header: 'Redberrian Insights',
                    body: 'We were soo much fun before the pandemic started! Parties almost every weekend and lavish employee birthday celebrations! Unfortunately, covid ruined that fun like it did almost everything else in the world. But we try our best to zhuzh it up a bit. For example, we host biweekly Devtalks where our senior and lead developers talk about topics they are passionate about. Previous topics have included Web3, NFT, Laravel 9, Kubernetes, etc. We hold these talks in the office but you can join our Zoom broadcast as well. Feel free to join either as an attendee or a speaker!'
                }

            default:
                return null;
        }
    };


    return (

        <>
            {activeStep === 4 ?
                <SubmitPage setActiveStep={setActiveStep} formData={formData} /> :
                <div className="redberry_form">
                    <div className='form__wrapper'>
                        <FormLayout
                            header={getStepForm(activeStep)?.header}
                            children={getStepForm(activeStep)?.component}
                            setActiveStep={setActiveStep}
                            activeStep={activeStep}
                        />
                    </div>
                    <div className='info__wrapper'>
                        <InfoLayout header={getStepInfo(activeStep)?.header}
                            body={getStepInfo(activeStep)?.body}
                        />
                    </div>
                </div>}
        </>
    )
}

export default Form